# Copyright (C) 2018 f <f@kefir.red>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

require 'sinatra'
require 'pony'
require 'email_address'

set :protection, false

get '/' do
  send_file 'README.md', type: 'text/plain; charset=utf-8'
end

post '/send' do
  # Este archivo es una lista de dominios a los que estamos permitiendo
  # enviar correo
  authorized_hosts = File.read('authorized_hosts').split("\n")

  from = params[:from]
  host = params.dig(:site)
  to   = "contacto@#{host}"
  site = "http://#{host || 'kefir.red'}"

  # Redirigir a una gzip bomb si no está autorizado
  unless authorized_hosts.include? host
    headers 'Content-Encoding' => 'gzip'
    send_file 'bomb.gz', type: 'text/html'
    halt
  end

  # Reenviar al sitio de origen si la dirección no es válida
  redirect site && halt unless EmailAddress.valid? from
  redirect site && halt unless EmailAddress.valid? to

  subject = "[#{site}] #{params.dig(:subject)}"
  body    = "Nombre: #{params.dig(:name)}\nTeléfono: #{params.dig(:phone)}\n\n#{params.dig(:body)}"
  # Emula una tabla de direcciones virtuales de postfix (!)
  map     = Hash[File.read('virtual').split("\n").map { |l| l.split(/\s+/, 2) }]

  # Enviamos localmente en lugar de hacernos pasar por la persona que
  # contacta.  La respuesta llega donde corresponde.
  Thread.new do
    Pony.mail to: map.fetch(host, to).split(',').map(&:strip),
      from: to,
      reply_to: from,
      via: :sendmail,
      body: body,
      subject: subject
  end

  redirect site
end
