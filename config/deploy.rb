# frozen_string_literal: true
set :application, 'contact-form'
set :repo_url, 'https://0xacab.org/itacate-kefir/contact-form.git'

set :bundle_flags, '--deployment'
set :default_env, path: '/usr/lib/passenger/bin:$PATH'

set :linked_files, %w{authorized_hosts virtual}
